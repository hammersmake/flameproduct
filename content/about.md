The flame series and the book "Thought Miracles", is a contemporary range of modern spiritual tools the brainchild of metaphyscian Rebeckh Burns. With a Ph.D. in Metaphysics, Rebeckh realised through her research the dehabilitating effects of negative thoughts and became devoted to forging a way to empower people to think positively in their daily lives and to increase the potential of the minds energy.

Rebeckh has created a new form of self-development product, combining original music with profound affirmations, visualisations and meditations. Rebeckh is committed to developing this range of self-development products, making them available globally using innovative marketing techniques.

Rebeckh currently lives in Auckland , New Zealand with her husband and two children. Continually studying and practicing metaphysics as well as her other passion Yoga. Rebeckh is a certified Bikram Yoga teacher.
