Flame Products and the book "Thought Miracles" is the brainchild of metaphysician Rebeckh Burns Ph.D. One of the many things that sets the Flame Meditations, Visualisations and Affirmations apart are that the music and text represents an integration of words, rhythm, voice and melody. Using these tools will give you the power to think positively, to remove the barriers to prosperity, minimise stress and create pathways to love and confidence.

Today’s non-stop, high-stress world means our minds are constantly in the beta (unreceptive) state, where the mind’s mental energy fires neurons at random. The music and words of Flame Products will return your mind to the alpha (receptive) state, a state where the neurons fire in harmony, where the positive message of the affirmations can be absorbed effectively by the conscious and subconscious mind.

The Affirmations, Meditations and Visualisations are short and easy for your mind to process, and they have been researched to empower the individual to achieve their maximum potential in daily life situations. A combination of dynamic and uplifting original music with profound yet practical affirmations Wherever you are, whatever you are doing, Flame Affirmations will help you rediscover

the art of living based on the science of life.

All the Flame Products and book "Thought Miracles" are downloadable for your convienience.
