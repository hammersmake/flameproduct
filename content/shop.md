All our products and new releases are available for purchase from this website via MP3 and PDF download.

Currently our retail outlets are located in Australia and New Zealand. But we are always looking for new stores and distributors worldwide, so please contact us on enquiries@flameproducts.com.

Australia

"Thought Miracles - Affirmations to ignite your future" available at leading bookstores nationwide. Published by Harper Collins ISBN # 0-7322-7389-7

New Zealand

"Thought Miracles - Affirmations to ignite your future" ISBN # 0-7322-7389-7. Available at selected bookstores nationwide. Pathfinders Bookstore (Pathfinders stock all Flame Products and "Thought Miracles"), Lorne Street, Auckland City.
